const copy = require('copy-webpack-plugin')
const css  = require('mini-css-extract-plugin')
const html = require('html-webpack-plugin')
const path = require('path')

module.exports = {
  mode: 'production',
  entry: './src/main.ts',
  output: {
    path: path.resolve( __dirname, 'dist' ),
    filename: 'main.js',
  },
  resolve: {
    extensions: [ '.ts', '.js' ],
  },
  module: {
    rules: [
      {
        test: /\.ts/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: css.loader
          },
          'css-loader',
        ],
      },
      {
        test: /\.styl$/,
        use: [
          {
            loader: css.loader
          },
          'css-loader',
          'stylus-loader'
        ],
      },
      {
        test: /\.jpe?g|png|gif|svg$/,
        use: {
          loader: 'file-loader',
          options: {
            outputPath: 'assets/'
          }
        },
      }
    ]
  },
  plugins: [
    new css(),
    new html({
      template: './src/index.html',
      inject: true,
      chunks: ['main'],
      filename: 'index.html'
    }),
    new copy({
      patterns: [
        { from: path.resolve(__dirname, 'public'), to: path.resolve(__dirname, 'dist') },
      ],
    })
  ]
}
